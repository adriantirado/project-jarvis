# Project J.A.R.V.I.S

This project aims to create a specialized application of Multiple Object Tracking during hockey games at RPI using different implementations based of deep learning models.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

To run code for the different implementations, Anaconda must be installed.
To install Anaconda please refer to the official [Anaconda Website](https://www.anaconda.com/download/) and download the Python 3.7 version for your respective operating system. 


### Installing and Testing

#### OpenCV Implementation

To use the OpenCV based Implementation please first download the [Jarvis OpenCV Environment](Preliminary&#32;Implementations/opencv_implementation/jarvis_opencv_env.yml) and move it to your work directory. Afterwards, open your terminal in said directory and import the environment with the following command.

```
conda env create -f jarvis_opencv_env.yml
```

Once the environment setup is finished, activate the environment by using the following command.

```
activate jarvis_opencv_env
```

Now that our environment is setup and activated lets run a little demo to check it works! To run this demo please run the following command on your terminal
```
python yolo_opencv.py --classes classes.txt --config yolo.cfg --weights yolov3.weights --image hockey-demo.jpg
``` 

If your output looks like the image below you are all set!

![object_detection_demo](Preliminary&#32;Implementations/opencv_implementation/object-detection-demo.jpg)

## Built With

* [OpenCV](https://opencv.org/) - Computer Vision Library
* [Anaconda](https://anaconda.org/) - Dependency and Environment Management
* [Darknet](https://pjreddie.com/darknet/) - Model used for demo

## Authors

* **Development Goons** - *Initial work* - [PurpleBooth](https://gitlab.com/development-goons)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details